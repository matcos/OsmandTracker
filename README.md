Osmand-Tracker
=========

OsmAnd-Tracker is a small bit of PHP to accept location pings from
[OsmAnd](https://www.osmand.net) and display an interactive map with the recorded track and the current location. 


For visualization, the [Mapbox](https://www.mapbox.com/) JavaScript API is used. It is possible to record a track as well as display only the current location 

## Deployment instructions

Pull the project and copy all the files in your PHP server directory. Edit the settings file, to set $apikey with your Mapbox token. 

After your OsmandTracker server is online, configure the settings in the Osmand App. The full URL for your online tracking is:

http://example.org/tracker.php?lat={0}&lon={1}&timestamp={2}&hdop={3}&altitude={4}&speed={5}&key=SECRETKEY

Don't forget to replace example.org with your site AND set SECRETKEY to your
secret key!

## Contributions:

* [OsmAnd-Tracker from TooBee](https://github.com/ToeBee/OsmAnd-Tracker): The project is based on the OsmAnd-Tracker from TooBee, which has less functionality and does not use MapBox.
* [GreatVincentyDistance](https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php): The PHP snippet, to calculate the distance between to coordinates is originally from Stackoverflow. 

