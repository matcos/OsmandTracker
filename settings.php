<?php
    global $secretKey, $filePath, $accuracy, $minutediff, $distancediff, $zoom, $record;

    // This key must be entered in OsmAnd's tracking URL as &key=
    $secretKey = 'CHANGEME';

	// Token from MapBox.
	$apikey = "";


    /**Used to round the lat/lon values so you aren't sharing your *exact* location
    * 0 -> 111111 m difference
    * 2 -> 1111 m difference
    * 4 -> 11 m difference
    * 5 -> 1 m difference
    */
    $accuracy = 4; // in number of decimal positions


	// Where to store your last location
    $filePath = 'locations/';

    //when a new track should begein:
    //after xx minutes pause
    $minutediff = 60;
    //or xxxx meter distance between last location
    $distancediff = 1000;

    //zoom at the beginning
    $zoom = 13;

    //record track on/off
    $record = true; 
