<?php
    require "settings.php";
    require "distance.php";

	if ($key == 'CHANGEME') {
		die('Key not set.');
	}
    $key = $_GET['key'];
    if (md5($key . $secretKey) != md5($secretKey . $secretKey)) { // Constant time comparison
        print 'Invalid key';
        return;
    }

    //get all informations from the GET-values from Osmand
    $info['lat'] = round($_GET['lat'], $accuracy);
    $info['lon'] = round($_GET['lon'], $accuracy);
    $lon = $info['lon'];
    $lat = $info['lat'];
    $info['timestamp'] = intval($_GET['timestamp']);
    $info['hdop'] = floatval($_GET['hdop']);
    $info['altitude'] = floatval($_GET['altitude']);
    $info['speed'] = floatval($_GET['speed']);
    $timestampSeconds = round($info['timestamp']/1000,0);
    
    //get informations from last location
    $loc_string = file_get_contents($filePath.'lastloc.txt');
    $lastloc = unserialize($loc_string);
    $lastlat = $lastloc['lat'];
    $lastlon = $lastloc['lon'];

    if ($record){
        //read json data from the track file
        $json = file_get_contents($filePath.'track.geojson');
        //if empty, initialize
        if (empty($json)){
            $json = '{"type":"FeatureCollection","features":{}}';
        }
        //convert json to arrays
        $arr = json_decode($json, true);

        //calculate distance and minute difference to last location
        $lasttimestampSeconds = round($lastloc['timestamp']/1000,0);
        $minutesAgo = round(($timestampSeconds - $lasttimestampSeconds)/60,1);
        $distance = vincentyGreatCircleDistance($lat, $lon, $lastlat, $lastlon, 6371000);
        //array with new location
        $newline = array(0 => $lon ,1 => $lat );
        // calculate position of the last location
        $lastentry = count($arr[features]) -1;

        //if track data empty -> initialize
        if ($lastentry<0){
            $lastentry=0;
            $arr['features'][0]['type'] = "Feature";
            $arr['features'][0]['geometry']['type'] = "LineString";
        }

        //if minute or distance to big, start new section, initialize new section
        if (($minutesAgo >= $minutediff) || ($distance >= $distancediff)){
            $lastentry++;
            $arr['features'][$lastentry]['type'] = "Feature";
            $arr['features'][$lastentry]['geometry']['type'] = "LineString";
        }

        /*TODO if last section single -> make point*/

        //save new line to array
        $arr['features'][$lastentry]['geometry']['coordinates'][] = $newline;
        //encode to json
        $file = json_encode($arr);

        //save track
        file_put_contents($filePath.'track.geojson', $file);
    }
    //save last location
    file_put_contents($filePath.'lastloc.txt', serialize($info));

?>
