<?php
    require "settings.php";
		if ($secretKey == 'CHANGEME') {
			die('This application is not functional yet. See the README how to set it up (hint: edit settings.php).');
		}
		//get informations about last location
    $loc_string = file_get_contents($filePath.'lastloc.txt');
    $info = unserialize($loc_string);
    $timestampSeconds = round($info['timestamp']/1000,0);
    $minutesAgo = round((time() - $timestampSeconds)/60,1);
    $lat = $info['lat'];
    $lon = $info['lon'];
?>

    <!-- TODO reload button, which loads only the track -->
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Your title</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:0; bottom:0; width:100%; }
    </style>
</head>
<body>	

<style>
    #menu {
        position: absolute;
        background: #fff;
        padding: 10px;
        font-family: 'Open Sans', sans-serif;
    }
</style>
<!-- The map will be loaded here -->
<div id='map'></div>
<!-- One can choose between different layer -->
<div id='menu'>
    <input id='outdoors' type='radio' name='rtoggle' value='outdoors' checked='checked'>
    <label for='outdoors'>outdoor</label>
    <input id='satellite' type='radio' name='rtoggle' value='satellite'>
    <label for='satellite'>satellite</label>
</div>	
<script>

<?php
	echo 'mapboxgl.accessToken = "'.$apikey.'";';
?>
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/outdoors-v9',
    center: <?php echo '['.$lon.','.$lat.']'?>,
    zoom: <?php echo $zoom ?>
});

var layerList = document.getElementById('menu');
var inputs = layerList.getElementsByTagName('input');

function switchLayer(layer) {
    var layerId = layer.target.id;
    map.setStyle('mapbox://styles/mapbox/' + layerId + '-v9');
    map.on('style.load', function() {
        addLayerRoute(map);   //add custom layer and source to the new map style
    });
}



/*TODO point for start and end*/

/**
* adds the path and the current location to the map 
*/
function addLayerRoute(mapid) {
	var path = <?php echo "'".$filePath."'" ?>;
	var datapath= path + 'track.geojson';
  mapid.addLayer({
    "id": "route",
    "type": "line",
    "source": {
        "type": "geojson",
        "data": datapath
    },
    "layout": {
        "line-join": "round",
        "line-cap": "round"
    },
    "paint": {
        "line-color": "#3f86c6",
        "line-width": 8
    }
  });
	map.addLayer({
	    "id": "points",
	    "type": "symbol",
	    "source": {
	        "type": "geojson",
	        "data": {
	            "type": "FeatureCollection",
	            "features": [{
	                "type": "Feature",
	                "geometry": {
	                    "type": "Point",
	                    "coordinates": <?php echo '['.$lon.','.$lat.']'?>
	                },
	                "properties": {
	                    "title": "Location",
	                    "icon": "circle"
	                }
	            }]
	        }
	    },
	    "layout": {
	        "icon-image": "{icon}-15",
	        "text-field": "{title}",
	        "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
	        "text-offset": [0, 0.6],
	        "text-anchor": "top"
	    }
	});

};

for (var i = 0; i < inputs.length; i++) {
    inputs[i].onclick = switchLayer;
}

	// Add zoom and rotation controls to the map.
	map.addControl(new mapboxgl.NavigationControl());
	map.on('load', function () {
		addLayerRoute(map); 
	});
</script>

</body>
</html>